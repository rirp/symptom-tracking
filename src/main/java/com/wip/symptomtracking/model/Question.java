package com.wip.symptomtracking.model;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.wip.symptomtracking.commons.models.ModelId;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Document("question")
public class Question extends ModelId {
	private String question;

	private boolean isAntecedent;

	private boolean isDate;
	
	private Justification justification;
	
	private List<Answer> answers;

	private boolean isOpen = false;

	private Company company;

}
