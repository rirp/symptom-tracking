package com.wip.symptomtracking.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.wip.symptomtracking.model.City;
import com.wip.symptomtracking.model.Department;


public interface ICityRepository extends MongoRepository<City, Long> {
	List<City> findByCode(String code);
	List<City> findByDepartment(Department deparment);
}
