package com.wip.symptomtracking.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.wip.symptomtracking.model.Company;
import com.wip.symptomtracking.model.Question;

public interface IQuestionRepository extends MongoRepository<Question, Long> {
	List<Question> findByCompany(Company company);
}
