package com.wip.symptomtracking.model;

import org.springframework.data.mongodb.core.mapping.Document;

import com.wip.symptomtracking.commons.models.ModelId;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper=false)
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Document("company")
public class Company extends ModelId {
	private String nit;
	
	private String businessName;
	
	private String address;
	
	private String phoneNumber;
	
	private String analyticsUrl;
}