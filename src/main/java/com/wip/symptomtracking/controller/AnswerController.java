package com.wip.symptomtracking.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wip.symptomtracking.model.Answer;
import com.wip.symptomtracking.services.IAnswerServiceApi;
@RestController
@RequestMapping("/answer")
public class AnswerController {
	@Autowired
	private IAnswerServiceApi answerService;
	
	@PostMapping("create")
	public ResponseEntity<Answer> createAnswer(@RequestBody Answer request) {
		request = answerService.save(request);
		return ResponseEntity.created(null).body(request);
	}
}
