package com.wip.symptomtracking.dto;

import com.wip.symptomtracking.model.enumerables.DocumentType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder(toBuilder = true)
public class GetUserByDocumentRequestDTO {
	private DocumentType documentType;
	private String documentNumber;
}
