package com.wip.symptomtracking.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wip.symptomtracking.model.Department;
import com.wip.symptomtracking.services.IDepartmentServiceApi;


@RestController
@RequestMapping("department")
public class DepartmentController {

	@Autowired
	private IDepartmentServiceApi service;
	
	@PostMapping("create")
	public ResponseEntity<Department> create(@RequestBody Department request) {
		request = service.save(request);
		return ResponseEntity.created(null).body(request);
	}
	
	@GetMapping("/")	
	public ResponseEntity<List<Department>> findAll() {
		return ResponseEntity.ok(service.getAll());
	}
}
