package com.wip.symptomtracking.services;

import com.wip.symptomtracking.commons.IGenericServiceApi;
import com.wip.symptomtracking.model.Company;

public interface ICompanyServiceApi extends IGenericServiceApi<Company, Long>{
	Company findByNit(String nit);	
}
