package com.wip.symptomtracking.services;

import java.util.List;

import com.wip.symptomtracking.commons.IGenericServiceApi;
import com.wip.symptomtracking.model.UserQuestionAnswer;

public interface IUserQuestionAnswerServiceApi extends IGenericServiceApi<UserQuestionAnswer, Long>{
	List<UserQuestionAnswer> findByEmployeeId(long employeeId);
}
