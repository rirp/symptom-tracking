package com.wip.symptomtracking.dto;

import java.util.Date;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.wip.symptomtracking.commons.models.ErrorMessage;
import com.wip.symptomtracking.model.enumerables.DocumentType;
import com.wip.symptomtracking.model.enumerables.UserType;
import com.wip.symptomtracking.validations.EnumValidator;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder(toBuilder = true)
public class EmployeeRequestDTO {
	@EnumValidator(
			enumClazz = UserType.class, 
			message = ErrorMessage.ENUM_VALUE)
	private UserType userType;
	
	@EnumValidator(
			enumClazz = DocumentType.class, 
			message = ErrorMessage.ENUM_VALUE)
	private DocumentType documentType;
	
	private String username;
	
	private String password;

	@NotBlank(message = ErrorMessage.NO_EMPTY)
	private String documentNumber;
	
	@NotBlank(message = ErrorMessage.NO_EMPTY)
	private String firstName;

	private String secondName;

	@NotBlank(message = ErrorMessage.NO_EMPTY)
	private String lastName;

	private String surname;

	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@NotBlank(message = ErrorMessage.NO_EMPTY)
	private Date birthDay;

	@NotBlank(message = ErrorMessage.NO_EMPTY)
	private String gender;

	@NotBlank(message = ErrorMessage.NO_EMPTY)
	private String address;
	
	private long jobPositionId;
}
