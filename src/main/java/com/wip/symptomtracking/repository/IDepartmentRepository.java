package com.wip.symptomtracking.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.wip.symptomtracking.model.Department;


public interface IDepartmentRepository extends MongoRepository<Department, Long> {
	Department findByCode(String code);
}
