package com.wip.symptomtracking.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wip.symptomtracking.model.Employee;
import com.wip.symptomtracking.model.JobPosition;
import com.wip.symptomtracking.services.IEmployeeServiceApi;
import com.wip.symptomtracking.services.IJobPositionServiceApi;

@RestController
@RequestMapping("user")
public class UserController {
	@Autowired
	private IEmployeeServiceApi employeeService;
	
	@Autowired
	private IJobPositionServiceApi jobPositionService;
	
	@PostMapping("create")
	public ResponseEntity<Employee> create(@RequestBody Employee request) {
		JobPosition jobPosition = jobPositionService.get(request.getId());
		
		if (jobPosition == null) {
			throw new DataIntegrityViolationException("JobPosition does not exist in the data base"); 
		}
		
		request.setJobPosition(jobPosition);
		
		request = employeeService.save(request);
		return ResponseEntity.created(null).body(request);
	}
}
