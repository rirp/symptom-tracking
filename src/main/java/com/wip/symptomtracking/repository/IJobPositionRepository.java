package com.wip.symptomtracking.repository;


import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.wip.symptomtracking.model.Company;
import com.wip.symptomtracking.model.JobPosition;

public interface IJobPositionRepository extends MongoRepository<JobPosition, Long> {
	List<JobPosition> findByName(String name);
	List<JobPosition> findByCompany(Company company);
	List<JobPosition> findByCompanyAndName(Company company, String name);
}