package com.wip.symptomtracking.services;

import com.wip.symptomtracking.commons.IGenericServiceApi;
import com.wip.symptomtracking.model.Answer;


public interface IAnswerServiceApi extends IGenericServiceApi<Answer, Long>{

}
