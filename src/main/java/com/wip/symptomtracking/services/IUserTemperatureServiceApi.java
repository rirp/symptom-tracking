package com.wip.symptomtracking.services;

import com.wip.symptomtracking.commons.IGenericServiceApi;
import com.wip.symptomtracking.model.UserTemperature;

public interface IUserTemperatureServiceApi extends IGenericServiceApi<UserTemperature, Long> {
	
}
