package com.wip.symptomtracking.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.security.sasl.AuthenticationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wip.symptomtracking.dto.LoginRequestDTO;
import com.wip.symptomtracking.dto.LoginResponseDTO;
import com.wip.symptomtracking.model.Company;
import com.wip.symptomtracking.model.Employee;
import com.wip.symptomtracking.model.JobPosition;
import com.wip.symptomtracking.model.Question;
import com.wip.symptomtracking.services.ICompanyServiceApi;
import com.wip.symptomtracking.services.IEmployeeServiceApi;
import com.wip.symptomtracking.services.IJobPositionServiceApi;
import com.wip.symptomtracking.services.IQuestionServiceApi;

@RestController
@RequestMapping("/auth")
public class AuthenticationController {
	@Autowired
	private IEmployeeServiceApi employeeService;
	
	@Autowired
	private IJobPositionServiceApi jobPositionService;
	
	@Autowired
	private IQuestionServiceApi questionService;
	
	@Autowired
	private ICompanyServiceApi companyService;
	
	@PostMapping("login")
	public ResponseEntity<LoginResponseDTO> login(@RequestBody LoginRequestDTO request) throws AuthenticationException {
		Employee user = employeeService.login(request.getUsername().toLowerCase(), request.getPassword());
		
		long compantyId = user.getJobPosition().getCompany().getId();
		
		Company company = companyService.get(compantyId);
		List<JobPosition> jps = jobPositionService.getAll();
		List<Question> questionByCompany = questionService.getAll();
		questionByCompany = questionByCompany.stream()
				.filter(j -> j.getCompany().getId() == compantyId)
				.collect(Collectors.toList());
		
		List<Question> byAntecedent = questionByCompany.stream()
				.filter(q -> q.isAntecedent())
				.collect(Collectors.toList()); 
		List<Question> byFollowUp = questionByCompany.stream()
				.filter(q -> !q.isAntecedent())
				.collect(Collectors.toList()); 
		jps = jps.stream()
				.filter(j -> j.getCompany().getId() == compantyId)
				.collect(Collectors.toList());
		
		LoginResponseDTO response = new LoginResponseDTO();
		response.setAnalyticsUrl(company.getAnalyticsUrl());
		response.setAntecedentQuestions(byAntecedent);
		response.setFollowUpQuestions(byFollowUp);
		response.setJobPositions(jps);
		response.setEmployeeId(user.getId());
		response.setUserType(user.getUserType());
		
		return ResponseEntity.ok(response);
	}
}
