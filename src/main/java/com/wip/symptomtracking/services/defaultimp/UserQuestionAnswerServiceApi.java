package com.wip.symptomtracking.services.defaultimp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Service;

import com.wip.symptomtracking.commons.GenericServiceApi;
import com.wip.symptomtracking.model.UserQuestionAnswer;
import com.wip.symptomtracking.repository.IUserQuestionAnswerRepository;
import com.wip.symptomtracking.services.IUserQuestionAnswerServiceApi;

@Service
public class UserQuestionAnswerServiceApi extends GenericServiceApi<UserQuestionAnswer, Long>
		implements IUserQuestionAnswerServiceApi {

	@Autowired
	private IUserQuestionAnswerRepository userQuestionAnswerRepository;

	@Override
	public MongoRepository<UserQuestionAnswer, Long> getDao() {
		return userQuestionAnswerRepository;
	}

	@Override
	public UserQuestionAnswer save(UserQuestionAnswer entity) {
		entity.setId(getNextSequence("userQuestionAnswerSeq"));
		return super.save(entity);
	}

	@Override
	public List<UserQuestionAnswer> findByEmployeeId(long employeeId) {
		return userQuestionAnswerRepository.findByEmployeeId(employeeId);
	}
}
