package com.wip.symptomtracking.commons;

import java.io.Serializable;
import java.util.List;

public interface IGenericServiceApi<T, ID extends Serializable> {
	T save(T entity);

	T get(ID id);

	List<T> getAll();

	void delete(ID id);

	int getNextSequence(String seqName);
}
