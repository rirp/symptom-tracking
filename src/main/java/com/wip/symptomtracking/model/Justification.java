package com.wip.symptomtracking.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


@Data
@EqualsAndHashCode(callSuper=false)
@NoArgsConstructor
@AllArgsConstructor
public class Justification {
	private long answerIdToRequest;
	
	private String text;
	
	private boolean isDate;

}
