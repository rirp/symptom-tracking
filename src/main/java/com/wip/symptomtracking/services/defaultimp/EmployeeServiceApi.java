package com.wip.symptomtracking.services.defaultimp;

import javax.security.sasl.AuthenticationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Service;

import com.wip.symptomtracking.commons.GenericServiceApi;
import com.wip.symptomtracking.commons.models.ErrorMessage;
import com.wip.symptomtracking.exceptions.dto.AuthenticationExceptionCode;
import com.wip.symptomtracking.model.Employee;
import com.wip.symptomtracking.model.enumerables.DocumentType;
import com.wip.symptomtracking.repository.IEmployeeRepository;
import com.wip.symptomtracking.services.IEmployeeServiceApi;

@Service
public class EmployeeServiceApi extends GenericServiceApi<Employee, Long> implements IEmployeeServiceApi {
	private static final int USER_NOT_EXIST = 1;
	private static final int PASSWORD_NOT_MATCH = 2;

	@Autowired
	private IEmployeeRepository repository;

	@Override
	public MongoRepository<Employee, Long> getDao() {
		return repository;
	}

	@Override
	public Employee save(Employee entity) {
		if (entity.getUsername() != null) {
			Employee result = repository.findByUsername(entity.getUsername());
			if (result != null) {
				throw new DataIntegrityViolationException(ErrorMessage.ENTRY_ALREADY_EXIST);
			}
		}

		Employee result = repository.findByDocumentTypeAndDocumentNumber(entity.getDocumentType(),
				entity.getDocumentNumber());

		if (result != null) {
			throw new DataIntegrityViolationException(ErrorMessage.ENTRY_ALREADY_EXIST);
		}

		entity.setId(this.getNextSequence("employeeSeqs"));

		return super.save(entity);
	}

	@Override
	public Employee login(String username, String password) throws AuthenticationException {
		if (username == null || username.isEmpty()) {
			throw new AuthenticationExceptionCode(ErrorMessage.AUTHENTICATION_FAIL, USER_NOT_EXIST);
		}

		Employee user = repository.findByUsername(username);

		if (user == null) {
			throw new AuthenticationExceptionCode(ErrorMessage.AUTHENTICATION_FAIL, USER_NOT_EXIST);
		} else if (!user.getPassword().equals(password)) {
			throw new AuthenticationExceptionCode(ErrorMessage.AUTHENTICATION_FAIL, PASSWORD_NOT_MATCH);
		}

		return user;
	}

	@Override
	public Employee findByDocumentTypeAndDocumentNumber(DocumentType documentType, String documentNumber) {
		Employee employee = repository.findByDocumentTypeAndDocumentNumber(documentType, documentNumber);
		if (employee == null) {
			throw new DataIntegrityViolationException("No existe un usuario asociado a este documento");
		}
		return employee;
	}

}
