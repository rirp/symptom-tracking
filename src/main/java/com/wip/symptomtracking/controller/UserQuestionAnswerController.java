package com.wip.symptomtracking.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wip.symptomtracking.dto.SaveAnswerListRequestDTO;
import com.wip.symptomtracking.dto.SaveAnswerRequestDTO;
import com.wip.symptomtracking.dto.UserQuestionAnswerRequestDTO;
import com.wip.symptomtracking.model.Answer;
import com.wip.symptomtracking.model.Employee;
import com.wip.symptomtracking.model.Question;
import com.wip.symptomtracking.model.UserQuestionAnswer;
import com.wip.symptomtracking.services.IEmployeeServiceApi;
import com.wip.symptomtracking.services.IQuestionServiceApi;
import com.wip.symptomtracking.services.IUserQuestionAnswerServiceApi;

@RestController
@RequestMapping("userQuestionAnswer")
public class UserQuestionAnswerController {
	Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private IUserQuestionAnswerServiceApi service;

	@Autowired
	private IQuestionServiceApi questionService;

	@Autowired
	private IEmployeeServiceApi employeeService;

	@PostMapping("create/list/")
	public ResponseEntity<List<UserQuestionAnswer>> createList(@RequestBody SaveAnswerListRequestDTO request) {
		List<UserQuestionAnswer> answers = new ArrayList<>();
		System.out.println("hola: " + request.toString());
		for(SaveAnswerRequestDTO answer: request.getAnswerList()) {
			UserQuestionAnswerRequestDTO data = new UserQuestionAnswerRequestDTO();
			data.setAnswer(answer);
			data.setEmployeeId(request.getEmployeeId());
			
			answers.add(create(data).getBody());
		}
		return ResponseEntity.created(null).body(answers);
	}
	
	@PostMapping("create")
	public ResponseEntity<UserQuestionAnswer> create(@RequestBody UserQuestionAnswerRequestDTO request) {
		Employee employee = employeeService.get(request.getEmployeeId());

		if (employee == null) {
			throw new DataIntegrityViolationException("Employee does not exist in the data base");
		}

		Question question = questionService.get(request.getAnswer().getQuestionId());

		if (question == null) {
			throw new DataIntegrityViolationException("Question does not exist in the data base");
		}

		UserQuestionAnswer response = new UserQuestionAnswer();
		response.setQuestion(question);
		response.setEmployeeId(request.getEmployeeId());

		if (question.isOpen()) {
			if (request.getAnswer().getAnswerText() == null) {
				throw new DataIntegrityViolationException("The question is open and answerText must not be null");
			}

			response.setAnswerText(request.getAnswer().getAnswerText());
		} else {
			List<Answer> answers = question.getAnswers();
			boolean answerExits = false;

			for (int index = 0; index < answers.size(); index++) {
				if (answers.get(index).getId() == request.getAnswer().getAnswerSelected()) {
					answerExits = true;
					break;
				}
			}

			if (answerExits == false) {
				throw new DataIntegrityViolationException(
						"The answers selected does not match with the question's answers");
			}
			response.setJustification(request.getAnswer().getJustification());
			response.setAnswerSelected(request.getAnswer().getAnswerSelected());
		}

		response = service.save(response);

		return ResponseEntity.created(null).body(response);
	}

	@GetMapping("findAllUserAnswer")	
	public ResponseEntity<List<UserQuestionAnswer>> getAnswerByEmployee() {
				
		return ResponseEntity.ok(service.getAll());
	}

}
