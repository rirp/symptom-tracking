package com.wip.symptomtracking.model.enumerables;

import lombok.Getter;

public enum DocumentType {
	CC("CC"),
	CE("CE"),
	PA("PA"),
	PE("PE");
	
	@Getter
	private String value;
	
	DocumentType(String value) {
		this.value = value;
	}
}
