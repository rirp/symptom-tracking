package com.wip.symptomtracking.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wip.symptomtracking.commons.models.ModelId;
import com.wip.symptomtracking.dto.EmployeeRequestDTO;
import com.wip.symptomtracking.dto.EmployeeResponseDTO;
import com.wip.symptomtracking.dto.GetUserByDocumentRequestDTO;
import com.wip.symptomtracking.model.Employee;
import com.wip.symptomtracking.model.JobPosition;
import com.wip.symptomtracking.model.UserQuestionAnswer;
import com.wip.symptomtracking.services.IEmployeeServiceApi;
import com.wip.symptomtracking.services.IJobPositionServiceApi;
import com.wip.symptomtracking.services.IUserQuestionAnswerServiceApi;

@RestController
@RequestMapping("employee")
public class EmployeeController {

	@Autowired
	private IEmployeeServiceApi service;

	@Autowired
	private IJobPositionServiceApi jobPositionService;
	
	@Autowired
	private IUserQuestionAnswerServiceApi userQuestionService;

	@PostMapping("create")
	public ResponseEntity<Employee> create(@RequestBody EmployeeRequestDTO request) {

		JobPosition jobPosition = jobPositionService.get(request.getJobPositionId());

		if (jobPosition == null) {
			throw new DataIntegrityViolationException("JobPosition does not exist");
		}

		Employee employee = new Employee();
		employee.setAddress(request.getAddress());
		employee.setBirthDay(request.getBirthDay());
		employee.setDocumentNumber(request.getDocumentNumber());
		employee.setDocumentType(request.getDocumentType());
		employee.setFirstName(request.getFirstName());
		employee.setGender(request.getGender());
		employee.setJobPosition(jobPosition);
		employee.setLastName(request.getLastName());
		employee.setPassword(request.getPassword());
		employee.setSecondName(request.getSecondName());
		employee.setSurname(request.getSurname());
		employee.setUsername(request.getUsername() == null ? null : request.getUsername().toLowerCase());
		employee.setUserType(request.getUserType());
		employee = service.save(employee);
		return ResponseEntity.created(null).body(employee);
	}

	@PostMapping("getById")
	public ResponseEntity<Employee> getById(@RequestBody ModelId request) {
		return ResponseEntity.ok(service.get(request.getId()));
	}

	@PostMapping("getByDocument")
	public EmployeeResponseDTO getByDocumentTypeAndDocumentNumber(@RequestBody GetUserByDocumentRequestDTO request) {
		Employee employee = (Employee) service.findByDocumentTypeAndDocumentNumber(
				request.getDocumentType(), 
				request.getDocumentNumber());
		List<UserQuestionAnswer> answers = userQuestionService.findByEmployeeId(employee.getId());
		EmployeeResponseDTO response = new EmployeeResponseDTO();
		response.setAddress(employee.getAddress());
		response.setBirthDay(employee.getBirthDay());
		response.setDocumentNumber(employee.getDocumentNumber());
		response.setDocumentType(employee.getDocumentType());
		response.setFirstName(employee.getFirstName());
		response.setGender(employee.getGender());
		response.setJobPosition(employee.getJobPosition());
		response.setLastName(employee.getLastName());
		response.setPassword(employee.getPassword());
		response.setSecondName(employee.getSecondName());
		response.setSurname(employee.getSurname());
		response.setUsername(employee.getUsername());
		response.setUserType(employee.getUserType());
		response.setId(employee.getId());
		response.setRequestAntecedent(CollectionUtils.isEmpty(answers));
		return response;

	}

}
