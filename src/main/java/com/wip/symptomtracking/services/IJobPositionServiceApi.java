package com.wip.symptomtracking.services;

import java.util.List;

import com.wip.symptomtracking.commons.IGenericServiceApi;
import com.wip.symptomtracking.model.Company;
import com.wip.symptomtracking.model.JobPosition;

public interface IJobPositionServiceApi extends IGenericServiceApi<JobPosition, Long>{
	List<JobPosition> getByName(String name);
	public List<JobPosition> findByCompany(Company company);
}

