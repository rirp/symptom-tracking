package com.wip.symptomtracking.model;

import java.util.Date;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.wip.symptomtracking.commons.models.ErrorMessage;
import com.wip.symptomtracking.commons.models.ModelId;
import com.wip.symptomtracking.model.enumerables.DocumentType;
import com.wip.symptomtracking.model.enumerables.UserType;
import com.wip.symptomtracking.validations.EnumValidator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Valid
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Document("employee")
public class Employee extends ModelId {
	@EnumValidator(enumClazz = UserType.class, message = ErrorMessage.ENUM_VALUE)
	private UserType userType;

	@EnumValidator(enumClazz = DocumentType.class, message = ErrorMessage.ENUM_VALUE)
	private DocumentType documentType;

	@NotBlank(message = ErrorMessage.NO_EMPTY)
	private String documentNumber;
	
	private String username;

	private String password;

	@NotBlank(message = ErrorMessage.NO_EMPTY)
	private String firstName;

	private String secondName;

	@NotBlank(message = ErrorMessage.NO_EMPTY)
	private String lastName;

	private String surname;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
	@NotBlank(message = ErrorMessage.NO_EMPTY)
	private Date birthDay;

	@NotBlank(message = ErrorMessage.NO_EMPTY)
	private String gender;

	@NotBlank(message = ErrorMessage.NO_EMPTY)
	private String address;

	@NotBlank(message = ErrorMessage.NO_EMPTY)
	private JobPosition jobPosition;
}
