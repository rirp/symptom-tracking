package com.wip.symptomtracking.services;

import java.util.List;


import com.wip.symptomtracking.commons.IGenericServiceApi;
import com.wip.symptomtracking.model.City;

public interface ICityServiceApi extends IGenericServiceApi<City, Long>{
	List<City> getByCode(String code);
}