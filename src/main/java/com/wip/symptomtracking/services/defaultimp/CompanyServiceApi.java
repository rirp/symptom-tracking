package com.wip.symptomtracking.services.defaultimp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Service;

import com.wip.symptomtracking.commons.GenericServiceApi;
import com.wip.symptomtracking.commons.models.ErrorMessage;
import com.wip.symptomtracking.model.Company;
import com.wip.symptomtracking.repository.ICompanyRepository;
import com.wip.symptomtracking.services.ICompanyServiceApi;

@Service
public class CompanyServiceApi extends GenericServiceApi<Company, Long> implements ICompanyServiceApi {

	@Autowired
	private ICompanyRepository companyRepository;

	@Override
	public MongoRepository<Company, Long> getDao() {
		return companyRepository;
	}
	
	@Override
	public Company save(Company entity) {
		Company company = findByNit(entity.getNit());
		
		if (company != null) {
			throw new DataIntegrityViolationException(ErrorMessage.ENTRY_ALREADY_EXIST);
		}
		
		entity.setId(getNextSequence("companySeq"));
		return super.save(entity);
	}

	@Override
	public Company findByNit(String nit) {
		return companyRepository.findByNit(nit);
	}

}
