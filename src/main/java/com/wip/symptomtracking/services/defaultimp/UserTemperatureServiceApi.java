package com.wip.symptomtracking.services.defaultimp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Service;

import com.wip.symptomtracking.commons.GenericServiceApi;
import com.wip.symptomtracking.model.UserTemperature;
import com.wip.symptomtracking.repository.IUserTemperatureRepository;
import com.wip.symptomtracking.services.IUserTemperatureServiceApi;

@Service
public class UserTemperatureServiceApi extends GenericServiceApi<UserTemperature, Long> implements IUserTemperatureServiceApi {
	@Autowired
	private IUserTemperatureRepository repository;
	
	@Override
	public MongoRepository<UserTemperature, Long> getDao() {
		return repository;
	}
	
	@Override
	public UserTemperature save(UserTemperature entity) {
		entity.setId(getNextSequence("userTemperatureSeq"));
		return super.save(entity);
	}

}
