package com.wip.symptomtracking.services.defaultimp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Service;

import com.wip.symptomtracking.commons.GenericServiceApi;
import com.wip.symptomtracking.model.Answer;
import com.wip.symptomtracking.repository.IAnswerRepository;
import com.wip.symptomtracking.services.IAnswerServiceApi;

@Service
public class AnswerServiceApi extends GenericServiceApi<Answer, Long> implements IAnswerServiceApi {

	@Autowired
	private IAnswerRepository answerRepository;

	@Override
	public MongoRepository<Answer, Long> getDao() {
		return answerRepository;
	}

	@Override
	public Answer save(Answer entity) {
	
		entity.setId(getNextSequence("answerSeq"));
		return super.save(entity);
	}
}
