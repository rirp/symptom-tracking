package com.wip.symptomtracking.exceptions.dto;

import javax.security.sasl.AuthenticationException;

@SuppressWarnings("serial")
public class AuthenticationExceptionCode extends AuthenticationException {
	public AuthenticationExceptionCode() {
        super();
    }
	
	private int errorCode;
	
	public AuthenticationExceptionCode(String detail, int errorCode) {
		super(detail);
		this.errorCode = errorCode;
	}
	
	public int getErrorCode() {
		return errorCode;
	}
}
