package com.wip.symptomtracking.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder(toBuilder = true)
public class SaveAnswerRequestDTO {
	
	private long questionId;

	private Long answerSelected;

	private String answerText;
	
	private String justification;
}
