package com.wip.symptomtracking.dto;

import java.util.List;

import com.wip.symptomtracking.model.JobPosition;
import com.wip.symptomtracking.model.Question;
import com.wip.symptomtracking.model.enumerables.UserType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder(toBuilder = true)
public class LoginResponseDTO {
	private UserType userType;
	private long employeeId;
	private String analyticsUrl;
	private List<JobPosition> jobPositions;
	private List<Question> antecedentQuestions;
	private List<Question> followUpQuestions;
}
