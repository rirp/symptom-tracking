package com.wip.symptomtracking.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wip.symptomtracking.dto.UserTemperatureRequestDTO;
import com.wip.symptomtracking.model.Employee;
import com.wip.symptomtracking.model.UserTemperature;
import com.wip.symptomtracking.services.IEmployeeServiceApi;
import com.wip.symptomtracking.services.IUserTemperatureServiceApi;

@RestController
@RequestMapping("userTemperature")
public class UserTemperatureController {
	@Autowired
	private IUserTemperatureServiceApi service;
	
	@Autowired
	private IEmployeeServiceApi userService;
	
	@PostMapping("create")
	public ResponseEntity<UserTemperature> save(@RequestBody UserTemperatureRequestDTO request) {
		Employee employee = userService.get(request.getEmployeeId());
		
		if (employee == null) {
			throw new DataIntegrityViolationException("Empleado no existe");
		}
		
		UserTemperature temp = new UserTemperature();
		temp.setTemperature(request.getTemperature());
		temp.setEmployeeId(request.getEmployeeId());
		
		temp = service.save(temp);
		
		return ResponseEntity.created(null).body(temp);
	}
}
