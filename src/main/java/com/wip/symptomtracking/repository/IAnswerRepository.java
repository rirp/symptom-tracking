package com.wip.symptomtracking.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.wip.symptomtracking.model.Answer;


public interface IAnswerRepository extends MongoRepository<Answer, Long> {

}
