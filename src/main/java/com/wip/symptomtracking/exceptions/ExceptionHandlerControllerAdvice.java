package com.wip.symptomtracking.exceptions;

import javax.servlet.http.HttpServletRequest;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.wip.symptomtracking.exceptions.dto.AuthenticationExceptionCode;
import com.wip.symptomtracking.exceptions.dto.ExceptionResponseDTO;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class ExceptionHandlerControllerAdvice extends ResponseEntityExceptionHandler {

	@ExceptionHandler(AuthenticationExceptionCode.class)
	public ResponseEntity<ExceptionResponseDTO> handleAuthenticationFailed(final AuthenticationExceptionCode exception,
			final HttpServletRequest request) {
		ExceptionResponseDTO response = new ExceptionResponseDTO();
		response.setMessage(exception.getMessage());
		response.setExceptionName(exception.getClass().getName());
		response.setErrorCode(exception.getErrorCode());
		return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(response);
	}

	@ExceptionHandler(DataIntegrityViolationException.class)
	public ResponseEntity<ExceptionResponseDTO> handleDataIntegrityViolationException(
			final DataIntegrityViolationException exception, final HttpServletRequest request) {
		ExceptionResponseDTO response = new ExceptionResponseDTO();
		response.setMessage(exception.getMessage());
		response.setExceptionName(exception.getClass().getName());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
	}
}
