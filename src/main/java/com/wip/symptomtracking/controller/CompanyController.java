package com.wip.symptomtracking.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wip.symptomtracking.model.Company;
import com.wip.symptomtracking.services.ICompanyServiceApi;

@RestController
@RequestMapping("company")
public class CompanyController {

	@Autowired
	private ICompanyServiceApi service;
	
	@PostMapping("create")
	public ResponseEntity<Company> create(@RequestBody Company request) {
		request = service.save(request);
		return ResponseEntity.created(null).body(request);
	}
	
	@GetMapping("/")	
	public ResponseEntity<List<Company>> findAll() {
		return ResponseEntity.ok(service.getAll());
	}
}


