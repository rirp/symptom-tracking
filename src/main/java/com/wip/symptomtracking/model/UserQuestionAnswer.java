package com.wip.symptomtracking.model;

import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.wip.symptomtracking.commons.models.ModelId;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper=false)
@NoArgsConstructor
@AllArgsConstructor
@Document("userQuestionAnswer")
public class UserQuestionAnswer extends ModelId {
	
	private long employeeId;
	
	private Question question;
	
	private long answerSelected;
	
	private String answerText;
	
	@JsonFormat(
			shape = JsonFormat.Shape.STRING, 
			pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
	private Date createdDate = new Date();
	
	private String justification;
}
