package com.wip.symptomtracking.services;

import com.wip.symptomtracking.commons.IGenericServiceApi;
import com.wip.symptomtracking.model.Department;

public interface IDepartmentServiceApi extends IGenericServiceApi<Department, Long>{
	Department findByCode(String code);
	
}
