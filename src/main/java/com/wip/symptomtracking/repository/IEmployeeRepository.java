package com.wip.symptomtracking.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.wip.symptomtracking.model.Employee;
import com.wip.symptomtracking.model.enumerables.DocumentType;

public interface IEmployeeRepository extends MongoRepository<Employee, Long> {
	Employee findByUsername(String username);
	
	List<Employee> findByDocumentNumber(String documentNumber);
	
	Employee findByDocumentTypeAndDocumentNumber(DocumentType documentType, String documentNumber);
}
