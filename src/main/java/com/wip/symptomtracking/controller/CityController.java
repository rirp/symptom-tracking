package com.wip.symptomtracking.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wip.symptomtracking.dto.CityRequestDTO;
import com.wip.symptomtracking.model.City;
import com.wip.symptomtracking.model.Department;
import com.wip.symptomtracking.services.ICityServiceApi;
import com.wip.symptomtracking.services.IDepartmentServiceApi;


@RestController
@RequestMapping ("city")
public class CityController {
	Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private ICityServiceApi service;
	
	@Autowired
	private IDepartmentServiceApi departmentService;
	
	
	@PostMapping("create")
	public ResponseEntity<City> create(@RequestBody CityRequestDTO request) {
		Department department = departmentService.get(request.getDepartmentId());
		
		if (department == null) {
			throw new DataIntegrityViolationException("Department does not exist in the data base");
		}
		
		logger.debug("Department found: ", department.toString());
		
		
		City city = new City();
		city.setDepartment(department);
		city.setCode(request.getCityCode());
		city.setName(request.getCityName());
		
		city = service.save(city);
		return ResponseEntity.created(null).body(city);
	}
	
	@GetMapping("/")	
	public ResponseEntity<List<City>> findAll() {
		return ResponseEntity.ok(service.getAll());
	}
}

