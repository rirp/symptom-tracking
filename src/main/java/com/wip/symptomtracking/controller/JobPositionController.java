package com.wip.symptomtracking.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wip.symptomtracking.dto.JobPositionRequestDTO;
import com.wip.symptomtracking.model.Company;
import com.wip.symptomtracking.model.JobPosition;
import com.wip.symptomtracking.services.ICompanyServiceApi;
import com.wip.symptomtracking.services.IJobPositionServiceApi;

@RestController
@RequestMapping("job-position")
public class JobPositionController {
	Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private IJobPositionServiceApi service;

	@Autowired
	private ICompanyServiceApi companyService;

	@PostMapping("create")
	public ResponseEntity<JobPosition> create(@RequestBody JobPositionRequestDTO request) {
		Company company = companyService.get(request.getCompanyId());

		if (company == null) {
			throw new DataIntegrityViolationException("Company does not exist in the data base");
		}

		logger.debug("Company found: ", company.toString());

		JobPosition jobPosition = new JobPosition();
		jobPosition.setCompany(company);
		jobPosition.setName(request.getJobPositionName());

		jobPosition = service.save(jobPosition);
		return ResponseEntity.created(null).body(jobPosition);
	}

	@PostMapping("company/{companyId}")
	public ResponseEntity<List<JobPosition>> getByCompany(@PathVariable(value = "companyId") long companyId) {
		Company company = companyService.get(companyId);

		if (company == null) {
			throw new DataIntegrityViolationException("Company does not exist in the data base");
		}

		return ResponseEntity.ok(service.findByCompany(company));
	}

	@GetMapping("/")
	public ResponseEntity<List<JobPosition>> findAll() {
		return ResponseEntity.ok(service.getAll());
	}
}
