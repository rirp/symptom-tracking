package com.wip.symptomtracking.model;

import org.springframework.data.mongodb.core.mapping.Document;

import com.wip.symptomtracking.commons.models.ModelId;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper=false)
@NoArgsConstructor
@AllArgsConstructor
@Document("city")
public class City extends ModelId {
	private String code;
	
	private String name;
	
	private Department department;
}
