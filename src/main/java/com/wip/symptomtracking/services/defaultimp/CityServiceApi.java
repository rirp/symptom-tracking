package com.wip.symptomtracking.services.defaultimp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.wip.symptomtracking.commons.GenericServiceApi;
import com.wip.symptomtracking.commons.models.ErrorMessage;
import com.wip.symptomtracking.model.City;
import com.wip.symptomtracking.repository.ICityRepository;
import com.wip.symptomtracking.services.ICityServiceApi;

@Service
public class CityServiceApi extends GenericServiceApi<City, Long> implements ICityServiceApi {
	@Autowired
	private ICityRepository cityRepository;

	@Override
	public MongoRepository<City, Long> getDao() {
		return cityRepository;
	}
	
	@Override
	public List<City> getByCode(String Code) {
		return cityRepository.findByCode(Code);
	}

	@Override
	public City save(City entity) {		
		List <City> cityList = cityRepository.findByDepartment(entity.getDepartment());
		
		if(CollectionUtils.isEmpty(cityList)) {
			throw new DataIntegrityViolationException(ErrorMessage.ENTRY_ALREADY_EXIST);
		}
		
		entity.setId(getNextSequence("citySeq"));
		
		return super.save(entity);
	}
}
