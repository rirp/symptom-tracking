package com.wip.symptomtracking.dto;

import javax.validation.Valid;

import org.springframework.data.mongodb.core.mapping.Document;

import com.wip.symptomtracking.model.Employee;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Valid
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Document("employee")
public class EmployeeResponseDTO extends Employee {
	private boolean requestAntecedent;
}
