package com.wip.symptomtracking.services;

import javax.security.sasl.AuthenticationException;

import com.wip.symptomtracking.commons.IGenericServiceApi;
import com.wip.symptomtracking.model.Employee;
import com.wip.symptomtracking.model.enumerables.DocumentType;

public interface IEmployeeServiceApi extends IGenericServiceApi<Employee, Long> {
	Employee login(String username, String password) throws AuthenticationException;
	
	public Employee findByDocumentTypeAndDocumentNumber(DocumentType documentType, String documentNumber);
	 
}
