package com.wip.symptomtracking.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.wip.symptomtracking.model.Company;

public interface ICompanyRepository extends MongoRepository<Company, Long> {
	Company findByNit(String nit);

}
