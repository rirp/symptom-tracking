package com.wip.symptomtracking.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder(toBuilder = true)
public class SaveAnswerListRequestDTO {
	private long employeeId;
	
	private List<SaveAnswerRequestDTO> answerList;
}
