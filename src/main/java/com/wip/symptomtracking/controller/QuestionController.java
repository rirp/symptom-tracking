package com.wip.symptomtracking.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wip.symptomtracking.dto.QuestionRequestDTO;
import com.wip.symptomtracking.model.Answer;
import com.wip.symptomtracking.model.Company;
import com.wip.symptomtracking.model.Question;
import com.wip.symptomtracking.services.IAnswerServiceApi;
import com.wip.symptomtracking.services.ICompanyServiceApi;
import com.wip.symptomtracking.services.IQuestionServiceApi;

@RestController
@RequestMapping("question")
public class QuestionController {
	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private IAnswerServiceApi answerService;

	@Autowired
	private IQuestionServiceApi service;

	@Autowired
	private ICompanyServiceApi companyService;

	@PostMapping("create")
	public ResponseEntity<Question> createQuestion(@RequestBody QuestionRequestDTO request) {
		Company company = companyService.get(request.getCompanyId());

		if (company == null) {
			throw new DataIntegrityViolationException("Company does not exist in the data base");
		}

		logger.debug("Company found: ", company.toString());

		Question question = new Question();
		question.setCompany(company);
		
		if (request.isOpen()) {
			question.setJustification(null);
			question.setAnswers(null);
		}

		if (request.getAnswerOptions() != null && !request.getAnswerOptions().isEmpty()) {
			question.setJustification(request.getJustification());
			Set<Long> answers = request.getAnswerOptions();
			List<Answer> answerList = new ArrayList<>();

			for (Long answerId : answers) {
				Answer answer = this.answerService.get(answerId);
				if (answer == null) {
					throw new DataIntegrityViolationException(
							"Answer with id=" + answerId + " does not exist in the data base");
				}
				answerList.add(answer);
			}

			question.setAnswers(answerList);
		}
		question.setQuestion(request.getQuestionText());
		question.setAntecedent(request.isAntecedent());
		question.setOpen(request.isOpen());
		question.setDate(request.isDate());
		question = service.save(question);
		return ResponseEntity.created(null).body(question);
	}

	@GetMapping("findAlll")
	public ResponseEntity<List<Question>> findAll() {
		return ResponseEntity.ok(service.getAll());
	}

	@GetMapping("company/{companyId}")
	public ResponseEntity<List<Question>> getQuestionsbyCompany(@PathVariable(value = "companyId") long companyId) {
		Company company = companyService.get(companyId);

		if (company == null) {
			throw new DataIntegrityViolationException("Company does not exist in the data base");
		}

		return ResponseEntity.ok(service.getByCompany(company));
	}
}
