package com.wip.symptomtracking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(
		basePackages = {
				"com.wip.symptomtracking"
		})
@SpringBootApplication
public class SymptomTrackingApplication {

	public static void main(String[] args) {
		SpringApplication.run(SymptomTrackingApplication.class, args);
	}

}
