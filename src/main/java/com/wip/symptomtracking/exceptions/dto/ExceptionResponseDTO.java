package com.wip.symptomtracking.exceptions.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExceptionResponseDTO {
	private String exceptionName;
	private String message;
	private int errorCode;
}
