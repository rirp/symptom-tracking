package com.wip.symptomtracking.dto;

import java.util.Set;

import com.wip.symptomtracking.model.Justification;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder(toBuilder = true)
public class QuestionRequestDTO {
	private String questionText;
	
	private Set<Long> answerOptions;
	
	private boolean isAntecedent;
	
	private boolean isDate;
	
	private Justification justification;

	@ApiModelProperty(example = "true")
	private boolean open;
	
	@ApiModelProperty(example = "1")
	private long CompanyId;
}
