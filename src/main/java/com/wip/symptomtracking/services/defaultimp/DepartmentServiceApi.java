package com.wip.symptomtracking.services.defaultimp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Service;

import com.wip.symptomtracking.commons.GenericServiceApi;
import com.wip.symptomtracking.commons.models.ErrorMessage;
import com.wip.symptomtracking.model.Department;
import com.wip.symptomtracking.repository.IDepartmentRepository;
import com.wip.symptomtracking.services.IDepartmentServiceApi;

@Service
public class DepartmentServiceApi extends GenericServiceApi<Department, Long> implements IDepartmentServiceApi {

	@Autowired
	private IDepartmentRepository departmentRepository;

	@Override
	public MongoRepository<Department, Long> getDao() {
		return departmentRepository;
	}

	@Override
	public Department save(Department entity) {
		Department department = findByCode(entity.getCode());

		if (department != null) {
			throw new DataIntegrityViolationException(ErrorMessage.ENTRY_ALREADY_EXIST);
		}

		entity.setId(getNextSequence("departmentSeq"));
		return super.save(entity);
	}

	@Override
	public Department findByCode(String code) {
		return departmentRepository.findByCode(code);
	}

}
