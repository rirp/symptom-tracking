package com.wip.symptomtracking.services.defaultimp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Service;

import com.wip.symptomtracking.commons.GenericServiceApi;
import com.wip.symptomtracking.commons.models.ErrorMessage;
import com.wip.symptomtracking.model.Company;
import com.wip.symptomtracking.model.Question;
import com.wip.symptomtracking.repository.IQuestionRepository;
import com.wip.symptomtracking.services.IQuestionServiceApi;

@Service
public class QuestionServiceApi extends GenericServiceApi<Question, Long> implements IQuestionServiceApi {

	@Autowired
	private IQuestionRepository questionRepository;

	@Override
	public MongoRepository<Question, Long> getDao() {
		return questionRepository;
	}

	@Override
	public Question save(Question entity) {
		Question question = get(entity.getId());

		if (question != null) {
			throw new DataIntegrityViolationException(ErrorMessage.ENTRY_ALREADY_EXIST);
		}

		entity.setId(getNextSequence("questionSeq"));
		return super.save(entity);
	}
	
	@Override
	public List<Question> getByCompany(Company company) {
		return questionRepository.findByCompany(company);
	}
}
