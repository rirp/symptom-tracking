package com.wip.symptomtracking.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.wip.symptomtracking.model.Question;
import com.wip.symptomtracking.model.UserQuestionAnswer;

public interface IUserQuestionAnswerRepository extends MongoRepository<UserQuestionAnswer, Long> {
	UserQuestionAnswer findByQuestionAndEmployeeId(Question question, long employeeId);
	List<UserQuestionAnswer> findByEmployeeId(long employeeId);
}
