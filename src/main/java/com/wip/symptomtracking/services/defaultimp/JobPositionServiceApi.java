package com.wip.symptomtracking.services.defaultimp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Service;

import com.wip.symptomtracking.commons.GenericServiceApi;
import com.wip.symptomtracking.commons.models.ErrorMessage;
import com.wip.symptomtracking.model.Company;
import com.wip.symptomtracking.model.JobPosition;
import com.wip.symptomtracking.repository.IJobPositionRepository;
import com.wip.symptomtracking.services.IJobPositionServiceApi;

@Service
public class JobPositionServiceApi extends GenericServiceApi<JobPosition, Long> implements IJobPositionServiceApi {
	@Autowired
	private IJobPositionRepository jobPositionRepository;

	@Override
	public MongoRepository<JobPosition, Long> getDao() {
		return jobPositionRepository;
	}

	@Override
	public List<JobPosition> getByName(String name) {
		return jobPositionRepository.findByName(name);
	}
	
	@Override
	public List<JobPosition> findByCompany(Company company) {
		return jobPositionRepository.findByCompany(company);
	}

	@Override
	public JobPosition save(JobPosition entity) {
		List <JobPosition> jobPositionList = jobPositionRepository.findByCompanyAndName(entity.getCompany(), entity.getName());
		
		if (jobPositionList != null && !jobPositionList.isEmpty()) {
			throw new DataIntegrityViolationException(ErrorMessage.ENTRY_ALREADY_EXIST);
		}
		
		entity.setId(getNextSequence("jobPositionSeq"));
		
		return super.save(entity);
	}
}
