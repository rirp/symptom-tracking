package com.wip.symptomtracking.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.wip.symptomtracking.model.UserTemperature;

public interface IUserTemperatureRepository extends MongoRepository<UserTemperature, Long>{

}
