package com.wip.symptomtracking.services;

import java.util.List;

import com.wip.symptomtracking.commons.IGenericServiceApi;
import com.wip.symptomtracking.model.Company;
import com.wip.symptomtracking.model.Question;

public interface IQuestionServiceApi extends IGenericServiceApi<Question, Long>{
	List<Question> getByCompany(Company company);
}
