package com.wip.symptomtracking.commons.models;

public class ErrorMessage {
	public static final String NO_EMPTY = "El valor no puede ser vacio";
	public static final String ENUM_VALUE = "This error is coming from the enum class";
	public static final String AUTHENTICATION_FAIL = "Usuario o contraseña están equivocados";
	public static final String ENTRY_ALREADY_EXIST = "El registro ya existe";
}
